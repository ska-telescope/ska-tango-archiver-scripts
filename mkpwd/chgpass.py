'''I do all the changes'''
from vault import vaultset
from pg import change
from helper import config, generate_password, getconf

def main():
    '''I'm the main'''
    params = config()
    addtopath=f"{params.environment}/{params.database}"
    pwd = generate_password(params.passwordLength)
    print(pwd)
    cfg = getconf(params.configFile)
    change(params.user, pwd, cfg, params.run)
    vaultset(params.user, pwd, cfg, extrapath=addtopath)
    print("done")

if __name__ == "__main__":
    main()
