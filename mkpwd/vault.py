'''I do stuff with the vault'''

import os
import logging
import hvac
from helper import getconf, config, VAULTTOKEN

def connect(cfg, vaulttoken=None):
    '''Create a vault connection with a token'''
    logging.debug("connecting")
    client = hvac.Client(url = cfg["vault"]["vaultUrl"],
                         token = vaulttoken)
    out = client.is_authenticated()
    logging.debug("done %s", out)
    return client

def vaultget(fullpath, keyname, cfg):
    '''I read stuff'''
    try:
        vaulttoken = os.environ[VAULTTOKEN]
    except KeyError:
        logging.debug("Missing %s environment variable. Skipping vault ops.", VAULTTOKEN)
        print(f"No {VAULTTOKEN}, I can't read from the vault.")
        return None
    client = connect(cfg, vaulttoken)
    logging.debug("getting path %s", fullpath)
    read_response = client.kv.v2.read_secret_version(path=fullpath,
                                                  mount_point=cfg["vault"]["mountPoint"],
                                                  raise_on_deleted_version=False)
    logging.debug(read_response)
    return read_response["data"]["data"][keyname]

def vaultset(kname, value, cfg, extrapath=None):
    '''I write stuff'''
    try:
        vaulttoken = os.environ[VAULTTOKEN]
    except KeyError:
        logging.debug("Missing %s environment variable. Skipping vault ops.", VAULTTOKEN)
        print(f"No {VAULTTOKEN}, no change done on the vault.")
        return None
    client = connect(cfg, vaulttoken)
    invalue = {kname: value}
    configpath = cfg["vault"]["keyPath"]
    if extrapath is not None:
        configpath = f"{configpath}/{extrapath}"
    logging.debug("setting a value on a path: %s, %s",invalue, configpath )
    write_response = client.secrets.kv.v2.create_or_update_secret(path=configpath,
                            mount_point=cfg["vault"]["mountPoint"],
                            secret=invalue)
    logging.debug(write_response)
    return write_response

def main():
    '''Read the config, connect to vault, read a key on keypath'''
    args = config()
    cfg = getconf(args.configFile)
    extrapath = f"{args.environment}/{args.database}"
    fullpath = f'{cfg["vault"]["keyPath"]}/{extrapath}'
    output=vaultget(fullpath, args.user, cfg)
    print(output)
    #newPass = generate_password(12)
    #print(newPass)
    #output=vaultset(newPass, cfg)
    #print(output)

if __name__ == "__main__":
    main()
