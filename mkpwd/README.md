# A tool to synch credentials from Vault

It currently works with:
* PostgreSQL

# How it works

There is one file that handles an implementation. 
* vault.py handles the vault get/set. When executed alone, it will return a value from the vault.
* pg.py handles the PostgreSQL password changes. When executed alone, it will change the password from the user.
* helper.py generates password in a secure way, read config files and options.
* chpass.py creates a new passowrd for an user and creates or changes it on vault. Then it's also changed on PostgreSQL. Your workflow might be sligthly different.
* vault.cfg is a configuration file with the vault and database configuration.

You can easily extend the tool.

Any vault operation requires a VAULT_TOKEN environment variable. 
If you want to try to do the password change on the database, use the --run option.


