'''I do stuff with PostgreSQL'''

import logging
import psycopg2
from psycopg2 import sql
from helper import getconf, generate_password, config


def change(user, passwstr, cfg, execute=True):
    '''I create the SQL and execute it to change user password'''
    logging.debug("Changing pass for %s ", user)
    sqlstmt = sql.SQL("ALTER USER {} PASSWORD %s").format(sql.Identifier(user))
    conn = psycopg2.connect(cfg["postgresql"]["dsn"])

    if execute:
        cursor = conn.cursor()
        cursor.execute(sqlstmt, [passwstr])
        logging.debug("Password changed")
        conn.commit()
        cursor.close()
        conn.close()
    else:
        passwstr = f"'{passwstr}';"
        print("--run this as db admin on target")
        print(sqlstmt.as_string(conn)%passwstr)

def main():
    '''I'm the main'''
    args = config()
    pwd = generate_password(args.passwordLength)
    cfg = getconf(args.configFile)
    print(f"{args.user}/{pwd}")
    change(args.user, pwd, cfg, args.run)

if __name__ == "__main__":
    main()
