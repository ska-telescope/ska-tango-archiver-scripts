'''Helper file'''
import configparser
import argparse
import string
import secrets

def generate_password(length, punctuation=True):
    '''I generate random secure strings'''
    characters = string.ascii_lowercase + string.ascii_uppercase + string.digits
    if punctuation:
        punct = string.punctuation.replace("'","").replace('"',"")
        characters = characters + punct
    password = ""
    for i in range(length):
        password += secrets.choice(characters)
    return password

def getconf(afile):
    "I read a config file"
    cfg = configparser.ConfigParser()
    cfg.read(afile)
    return cfg

VAULTTOKEN="VAULT_TOKEN"
PASSLENGTH=18
CONFIGFILE="vault.cfg"
USER="test"
ENV="laptop"
DB="postgresql"

def config():
    '''I get the default parameters'''
    parser = argparse.ArgumentParser(description="I change credential on database " +
                                     "and vault if a VAULT_TOKEN environment parameter exists.")
    parser.add_argument(
        "-u", "--user", default=USER, help=f"User, default is {USER}"
    )
    parser.add_argument("-c", "--configFile", default=CONFIGFILE,
                        help=f"Configuration file, default is {CONFIGFILE}")
    parser.add_argument("-p", "--passwordLength", default=PASSLENGTH,
                        help=f"Password length, default is {PASSLENGTH}")
    parser.add_argument("-e", "--environment", default=ENV,
                        help=f"Environment, default is {ENV}")
    parser.add_argument("-d", "--database", default=DB,
                        help=f"Database, default is {DB}")
    parser.add_argument("--run", action="store_true",
                        help="Run the command on a database if present")
    args = parser.parse_args()
    return args
