'''Helper'''
import sys
import os
import mariadb
from config import config, defaults

def read_params(db="mariadb"):
    """Read variables from the environment"""
    out = {}
    for k in config["params"]:
        invalue = os.getenv(k)
        if invalue is None:
            out[k] = defaults[db][k]
            print(f"Parameter {k} is missing!")
        else:
            out[k] = invalue
    return out

def test(params, db="mariadb"):
    """Test the connection"""
    query = defaults[db]["query"]
    conn = None
    if db == "mariadb":
        p = {"user": params["USERNAME"],
             "password": params["PASSWORD"],
             "host": params["HOST"],
             "database": params["DBNAME"]
             }
        conn = mariadb.connect(**p)
    if conn is not None:
        cursor = conn.cursor()
        cursor.execute(query)
        row = cursor.fetchone()
        print(*row, sep=' ')
        cursor.close()
        conn.close()
    else:
        print("Connection not ready")
        sys.exit(1)
