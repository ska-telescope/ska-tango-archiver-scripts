#!/usr/bin/python3
'''The script that is called'''
import sys
from helper import read_params, test

def main():
    '''I am the main'''
    cfg = read_params()
    test(cfg, sys.argv[1])

if __name__ == "__main__":
    main()
