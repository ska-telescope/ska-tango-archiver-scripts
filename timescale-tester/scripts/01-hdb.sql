--
-- PostgreSQL database dump
--

-- Dumped from database version 14.7 (Ubuntu 14.7-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.7 (Ubuntu 14.7-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: {{ username }}
--

CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
select pg_sleep(1);

--create SCHEMA public;
grant usage on schema public to {{ username }};
grant create on schema public to {{ username }};
grant create on schema public to admin;

--CREATE SCHEMA public;

ALTER SCHEMA public OWNER TO admin;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: {{ username }}
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: uchar; Type: DOMAIN; Schema: public; Owner: {{ username }}
--

CREATE DOMAIN public.uchar AS numeric(3,0)
	CONSTRAINT uchar_check CHECK (((VALUE >= (0)::numeric) AND (VALUE <= (255)::numeric)));


ALTER DOMAIN public.uchar OWNER TO {{ username }};

--
-- Name: ulong; Type: DOMAIN; Schema: public; Owner: {{ username }}
--

CREATE DOMAIN public.ulong AS numeric(10,0)
	CONSTRAINT ulong_check CHECK (((VALUE >= (0)::numeric) AND (VALUE <= ('4294967295'::bigint)::numeric)));


ALTER DOMAIN public.ulong OWNER TO {{ username }};

--
-- Name: ulong64; Type: DOMAIN; Schema: public; Owner: {{ username }}
--

CREATE DOMAIN public.ulong64 AS numeric(20,0)
	CONSTRAINT ulong64_check CHECK (((VALUE >= (0)::numeric) AND (VALUE <= '18446744073709551615'::numeric)));


ALTER DOMAIN public.ulong64 OWNER TO {{ username }};

--
-- Name: public.ushort; Type: DOMAIN; Schema: public; Owner: {{ username }}
--

CREATE DOMAIN public.ushort AS numeric(5,0)
	CONSTRAINT ushort_check CHECK (((VALUE >= (0)::numeric) AND (VALUE <= (65535)::numeric)));


ALTER DOMAIN public.ushort OWNER TO {{ username }};

--
-- Name: set_enum_label(); Type: FUNCTION; Schema: public; Owner: {{ username }}
--

CREATE FUNCTION public.set_enum_label() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    IF NEW.value_r IS NOT NULL THEN
        NEW.value_r_label := (SELECT enum_labels[NEW.value_r + 1] FROM att_parameter WHERE att_conf_id=NEW.att_conf_id ORDER BY recv_time DESC LIMIT 1);
    END IF;
    IF NEW.value_w IS NOT NULL THEN
        NEW.value_w_label := (SELECT enum_labels[NEW.value_w + 1] FROM att_parameter WHERE att_conf_id=NEW.att_conf_id ORDER BY recv_time DESC LIMIT 1);
    END IF;
    RETURN NEW;
END
$$;


ALTER FUNCTION public.  set_enum_label() OWNER TO {{ username }};

--
-- Name: set_enum_label_array(); Type: FUNCTION; Schema: public; Owner: {{ username }}
--

CREATE FUNCTION public.set_enum_label_array() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    IF NEW.value_r IS NOT NULL THEN
	WITH enum_labels AS (
		SELECT enum_labels FROM att_parameter WHERE att_conf_id=NEW.att_conf_id ORDER BY recv_time DESC limit 1
	)
        SELECT array_agg(res) FROM (SELECT enum_labels[UNNEST(NEW.value_r)+ 1] FROM enum_labels) as res INTO NEW.value_r_label;
    END IF;
    IF NEW.value_w IS NOT NULL THEN
	WITH enum_labels AS (
		SELECT enum_labels FROM att_parameter WHERE att_conf_id=NEW.att_conf_id ORDER BY recv_time DESC limit 1
	)
        SELECT array_agg(res) FROM (SELECT enum_labels[UNNEST(NEW.value_w)+ 1] FROM enum_labels) as res INTO NEW.value_w_label;
    END IF;
    RETURN NEW;
END
$$;


ALTER FUNCTION public.  set_enum_label_array() OWNER TO {{ username }};

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: att_array_devboolean; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devboolean (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r boolean[],
    value_w boolean[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devboolean OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devboolean; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devboolean IS 'Array Boolean Values Table';


--
-- Name: att_array_devdouble; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devdouble (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r double precision[],
    value_w double precision[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devdouble OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devdouble; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devdouble IS 'Array Double Values Table';


--
-- Name: att_array_devencoded; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devencoded (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r bytea[],
    value_w bytea[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devencoded OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devencoded; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devencoded IS 'Array DevEncoded Values Table';


--
-- Name: att_array_devenum; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devenum (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r_label text[],
    value_r smallint[],
    value_w_label text[],
    value_w smallint[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devenum OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devenum; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devenum IS 'Array Enum Values Table';


--
-- Name: att_array_devfloat; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devfloat (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r real[],
    value_w real[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devfloat OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devfloat; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devfloat IS 'Array Float Values Table';


--
-- Name: att_array_devlong; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devlong (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r integer[],
    value_w integer[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devlong OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devlong; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devlong IS 'Array Long Values Table';


--
-- Name: att_array_devlong64; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devlong64 (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r bigint[],
    value_w bigint[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devlong64 OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devlong64; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devlong64 IS 'Array Long64 Values Table';


--
-- Name: att_array_devshort; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devshort (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r smallint[],
    value_w smallint[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devshort OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devshort; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devshort IS 'Array Short Values Table';


--
-- Name: att_array_devstate; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devstate (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r integer[],
    value_w integer[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devstate OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devstate; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devstate IS 'Array State Values Table';


--
-- Name: att_array_devstring; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devstring (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r text[],
    value_w text[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devstring OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devstring; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devstring IS 'Array String Values Table';


--
-- Name: att_array_devuchar; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devuchar (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r public.uchar[],
    value_w public.uchar[],
    quality smallint,
    details json,
    att_error_desc_id integer
);


ALTER TABLE public.att_array_devuchar OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devuchar; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devuchar IS 'Array UChar Values Table';


--
-- Name: att_array_devulong; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devulong (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r public.ulong[],
    value_w public.ulong[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devulong OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devulong; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devulong IS 'Array ULong Values Table';


--
-- Name: att_array_devulong64; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devulong64 (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r public.ulong64[],
    value_w public.ulong64[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devulong64 OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devulong64; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devulong64 IS 'Array ULong64 Values Table';


--
-- Name: att_array_devushort; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_array_devushort (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r public.ushort[],
    value_w public.ushort[],
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_array_devushort OWNER TO {{ username }};

--
-- Name: TABLE public.att_array_devushort; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_array_devushort IS 'Array UShort Values Table';


--
-- Name: att_conf; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_conf (
    att_conf_id integer NOT NULL,
    att_name text NOT NULL,
    att_conf_type_id smallint NOT NULL,
    att_conf_format_id smallint NOT NULL,
    att_conf_write_id smallint NOT NULL,
    table_name text NOT NULL,
    cs_name text DEFAULT ''::text NOT NULL,
    domain text DEFAULT ''::text NOT NULL,
    family text DEFAULT ''::text NOT NULL,
    member text DEFAULT ''::text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    ttl integer,
    hide boolean DEFAULT false
);


ALTER TABLE public.att_conf OWNER TO {{ username }};

--
-- Name: TABLE public.att_conf; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_conf IS 'Attribute Configuration Table';


--
-- Name: att_conf_att_conf_id_seq; Type: SEQUENCE; Schema: public; Owner: {{ username }}
--

CREATE SEQUENCE public.att_conf_att_conf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.att_conf_att_conf_id_seq OWNER TO {{ username }};

--
-- Name: att_conf_att_conf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{ username }}
--

ALTER SEQUENCE att_conf_att_conf_id_seq OWNED BY att_conf.att_conf_id;


--
-- Name: att_conf_format; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_conf_format (
    att_conf_format_id integer NOT NULL,
    format text NOT NULL,
    format_num smallint NOT NULL
);


ALTER TABLE public.att_conf_format OWNER TO {{ username }};

--
-- Name: TABLE public.att_conf_format; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_conf_format IS 'Attribute format type';


--
-- Name: att_conf_format_att_conf_format_id_seq; Type: SEQUENCE; Schema: public; Owner: {{ username }}
--

CREATE SEQUENCE public.att_conf_format_att_conf_format_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.att_conf_format_att_conf_format_id_seq OWNER TO {{ username }};

--
-- Name: att_conf_format_att_conf_format_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{ username }}
--

ALTER SEQUENCE att_conf_format_att_conf_format_id_seq OWNED BY att_conf_format.att_conf_format_id;


--
-- Name: att_conf_type; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_conf_type (
    att_conf_type_id integer NOT NULL,
    type text NOT NULL,
    type_num smallint NOT NULL
);


ALTER TABLE public.att_conf_type OWNER TO {{ username }};

--
-- Name: TABLE public.att_conf_type; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_conf_type IS 'Attribute data type';


--
-- Name: att_conf_type_att_conf_type_id_seq; Type: SEQUENCE; Schema: public; Owner: {{ username }}
--

CREATE SEQUENCE public.att_conf_type_att_conf_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.att_conf_type_att_conf_type_id_seq OWNER TO {{ username }};

--
-- Name: att_conf_type_att_conf_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{ username }}
--

ALTER SEQUENCE att_conf_type_att_conf_type_id_seq OWNED BY att_conf_type.att_conf_type_id;


--
-- Name: att_conf_write; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_conf_write (
    att_conf_write_id integer NOT NULL,
    write text NOT NULL,
    write_num smallint NOT NULL
);


ALTER TABLE public.att_conf_write OWNER TO {{ username }};

--
-- Name: TABLE public.att_conf_write; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_conf_write IS 'Attribute write type';


--
-- Name: att_conf_write_att_conf_write_id_seq; Type: SEQUENCE; Schema: public; Owner: {{ username }}
--

CREATE SEQUENCE public.att_conf_write_att_conf_write_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.att_conf_write_att_conf_write_id_seq OWNER TO {{ username }};

--
-- Name: att_conf_write_att_conf_write_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{ username }}
--

ALTER SEQUENCE att_conf_write_att_conf_write_id_seq OWNED BY att_conf_write.att_conf_write_id;


--
-- Name: att_error_desc; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_error_desc (
    att_error_desc_id integer NOT NULL,
    error_desc text NOT NULL
);


ALTER TABLE public.att_error_desc OWNER TO {{ username }};

--
-- Name: TABLE public.att_error_desc; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_error_desc IS 'Error Description Table';


--
-- Name: att_error_desc_att_error_desc_id_seq; Type: SEQUENCE; Schema: public; Owner: {{ username }}
--

CREATE SEQUENCE public.att_error_desc_att_error_desc_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.att_error_desc_att_error_desc_id_seq OWNER TO {{ username }};

--
-- Name: att_error_desc_att_error_desc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{ username }}
--

ALTER SEQUENCE att_error_desc_att_error_desc_id_seq OWNED BY att_error_desc.att_error_desc_id;


--
-- Name: att_history; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_history (
    att_conf_id integer NOT NULL,
    att_history_event_id integer NOT NULL,
    event_time timestamp with time zone NOT NULL,
    details json
);


ALTER TABLE public.att_history OWNER TO {{ username }};

--
-- Name: TABLE public.att_history; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_history IS 'Attribute Configuration Events History Table';


--
-- Name: att_history_event; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_history_event (
    att_history_event_id integer NOT NULL,
    event text NOT NULL
);


ALTER TABLE public.att_history_event OWNER TO {{ username }};

--
-- Name: TABLE public.att_history_event; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_history_event IS 'Attribute history events description';


--
-- Name: att_history_event_att_history_event_id_seq; Type: SEQUENCE; Schema: public; Owner: {{ username }}
--

CREATE SEQUENCE public.att_history_event_att_history_event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.att_history_event_att_history_event_id_seq OWNER TO {{ username }};

--
-- Name: att_history_event_att_history_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{ username }}
--

ALTER SEQUENCE att_history_event_att_history_event_id_seq OWNED BY att_history_event.att_history_event_id;


--
-- Name: att_parameter; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_parameter (
    att_conf_id integer NOT NULL,
    recv_time timestamp with time zone NOT NULL,
    label text DEFAULT ''::text NOT NULL,
    unit text DEFAULT ''::text NOT NULL,
    standard_unit text DEFAULT ''::text NOT NULL,
    display_unit text DEFAULT ''::text NOT NULL,
    format text DEFAULT ''::text NOT NULL,
    archive_rel_change text DEFAULT ''::text NOT NULL,
    archive_abs_change text DEFAULT ''::text NOT NULL,
    archive_period text DEFAULT ''::text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    details json,
    enum_labels text[] DEFAULT ARRAY[]::text[] NOT NULL
);


ALTER TABLE public.att_parameter OWNER TO {{ username }};

--
-- Name: TABLE public.att_parameter; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_parameter IS 'Attribute configuration parameters';


--
-- Name: att_scalar_devboolean; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devboolean (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r boolean,
    value_w boolean,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devboolean OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devboolean; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devboolean IS 'Scalar Boolean Values Table';


--
-- Name: att_scalar_devdouble; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devdouble (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r double precision,
    value_w double precision,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devdouble OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devdouble; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devdouble IS 'Scalar Double Values Table';


--
-- Name: att_scalar_devencoded; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devencoded (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r bytea,
    value_w bytea,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devencoded OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devencoded; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devencoded IS 'Scalar DevEncoded Values Table';


--
-- Name: att_scalar_devenum; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devenum (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r_label text,
    value_r smallint,
    value_w_label text,
    value_w smallint,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devenum OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devenum; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devenum IS 'Scalar Enum Values Table';


--
-- Name: att_scalar_devfloat; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devfloat (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r real,
    value_w real,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devfloat OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devfloat; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devfloat IS 'Scalar Float Values Table';


--
-- Name: att_scalar_devlong; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devlong (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r integer,
    value_w integer,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devlong OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devlong; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devlong IS 'Scalar Long Values Table';


--
-- Name: att_scalar_devlong64; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devlong64 (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r bigint,
    value_w bigint,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devlong64 OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devlong64; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devlong64 IS 'Scalar Long64 Values Table';


--
-- Name: att_scalar_devshort; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devshort (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r smallint,
    value_w smallint,
    quality smallint,
    details json,
    att_error_desc_id integer
);


ALTER TABLE public.att_scalar_devshort OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devshort; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devshort IS 'Scalar Short Values Table';


--
-- Name: att_scalar_devstate; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devstate (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r integer,
    value_w integer,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devstate OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devstate; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devstate IS 'Scalar State Values Table';


--
-- Name: att_scalar_devstring; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devstring (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r text,
    value_w text,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devstring OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devstring; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devstring IS 'Scalar String Values Table';


--
-- Name: att_scalar_devuchar; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devuchar (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r public.uchar,
    value_w public.uchar,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devuchar OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devuchar; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devuchar IS 'Scalar UChar Values Table';


--
-- Name: att_scalar_devulong; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devulong (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r public.ulong,
    value_w public.ulong,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devulong OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devulong; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devulong IS 'Scalar ULong Values Table';


--
-- Name: att_scalar_devulong64; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devulong64 (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r public.ulong64,
    value_w public.ulong64,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devulong64 OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devulong64; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devulong64 IS 'Scalar ULong64 Values Table';


--
-- Name: att_scalar_devushort; Type: TABLE; Schema: public; Owner: {{ username }}
--

CREATE TABLE public.att_scalar_devushort (
    att_conf_id integer NOT NULL,
    data_time timestamp with time zone NOT NULL,
    value_r public.ushort,
    value_w public.ushort,
    quality smallint,
    att_error_desc_id integer,
    details json
);


ALTER TABLE public.att_scalar_devushort OWNER TO {{ username }};

--
-- Name: TABLE public.att_scalar_devushort; Type: COMMENT; Schema: public; Owner: {{ username }}
--

COMMENT ON TABLE public.att_scalar_devushort IS 'Scalar UShort Values Table';


--
-- Name: att_conf att_conf_id; Type: DEFAULT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf ALTER COLUMN att_conf_id SET DEFAULT nextval('public.att_conf_att_conf_id_seq'::regclass);


--
-- Name: att_conf_format att_conf_format_id; Type: DEFAULT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_format ALTER COLUMN att_conf_format_id SET DEFAULT nextval('public.att_conf_format_att_conf_format_id_seq'::regclass);


--
-- Name: att_conf_type att_conf_type_id; Type: DEFAULT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_type ALTER COLUMN att_conf_type_id SET DEFAULT nextval('public.att_conf_type_att_conf_type_id_seq'::regclass);


--
-- Name: att_conf_write att_conf_write_id; Type: DEFAULT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_write ALTER COLUMN att_conf_write_id SET DEFAULT nextval('public.att_conf_write_att_conf_write_id_seq'::regclass);


--
-- Name: att_error_desc att_error_desc_id; Type: DEFAULT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_error_desc ALTER COLUMN att_error_desc_id SET DEFAULT nextval('public.att_error_desc_att_error_desc_id_seq'::regclass);


--
-- Name: att_history_event att_history_event_id; Type: DEFAULT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_history_event ALTER COLUMN att_history_event_id SET DEFAULT nextval('public.att_history_event_att_history_event_id_seq'::regclass);


--
-- Data for Name: att_array_devboolean; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devboolean (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devdouble; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devdouble (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devencoded; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devencoded (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devenum; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devenum (att_conf_id, data_time, value_r_label, value_r, value_w_label, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devfloat; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devfloat (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devlong; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devlong (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devlong64; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devlong64 (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devshort; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devshort (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devstate; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devstate (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devstring; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devstring (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devuchar; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devuchar (att_conf_id, data_time, value_r, value_w, quality, details, att_error_desc_id) FROM stdin;
\.


--
-- Data for Name: att_array_devulong; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devulong (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devulong64; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devulong64 (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_array_devushort; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_array_devushort (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_conf; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_conf (att_conf_id, att_name, att_conf_type_id, att_conf_format_id, att_conf_write_id, table_name, cs_name, domain, family, member, name, ttl, hide) FROM stdin;
\.


--
-- Data for Name: att_conf_format; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_conf_format (att_conf_format_id, format, format_num) FROM stdin;
1	SCALAR	0
2	SPECTRUM	1
3	IMAGE	2
\.


--
-- Data for Name: att_conf_type; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_conf_type (att_conf_type_id, type, type_num) FROM stdin;
1	DEV_BOOLEAN	1
2	DEV_SHORT	2
3	DEV_LONG	3
4	DEV_FLOAT	4
5	DEV_DOUBLE	5
6	DEV_USHORT	6
7	DEV_ULONG	7
8	DEV_STRING	8
9	DEV_STATE	19
10	DEV_UCHAR	22
11	DEV_LONG64	23
12	DEV_ULONG64	24
13	DEV_ENCODED	28
14	DEV_ENUM	30
\.


--
-- Data for Name: att_conf_write; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_conf_write (att_conf_write_id, write, write_num) FROM stdin;
1	READ	0
2	READ_WITH_WRITE	1
3	WRITE	2
4	READ_WRITE	3
\.


--
-- Data for Name: att_error_desc; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_error_desc (att_error_desc_id, error_desc) FROM stdin;
\.


--
-- Data for Name: att_history; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_history (att_conf_id, att_history_event_id, event_time, details) FROM stdin;
\.


--
-- Data for Name: att_history_event; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_history_event (att_history_event_id, event) FROM stdin;
\.


--
-- Data for Name: att_parameter; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_parameter (att_conf_id, recv_time, label, unit, standard_unit, display_unit, format, archive_rel_change, archive_abs_change, archive_period, description, details, enum_labels) FROM stdin;
\.


--
-- Data for Name: att_scalar_devboolean; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devboolean (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devdouble; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devdouble (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devencoded; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devencoded (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devenum; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devenum (att_conf_id, data_time, value_r_label, value_r, value_w_label, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devfloat; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devfloat (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devlong; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devlong (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devlong64; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devlong64 (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devshort; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devshort (att_conf_id, data_time, value_r, value_w, quality, details, att_error_desc_id) FROM stdin;
\.


--
-- Data for Name: att_scalar_devstate; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devstate (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devstring; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devstring (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devuchar; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devuchar (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devulong; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devulong (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devulong64; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devulong64 (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Data for Name: att_scalar_devushort; Type: TABLE DATA; Schema: public; Owner: {{ username }}
--

COPY public.att_scalar_devushort (att_conf_id, data_time, value_r, value_w, quality, att_error_desc_id, details) FROM stdin;
\.


--
-- Name: att_conf_att_conf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: {{ username }}
--

SELECT pg_catalog.setval('public.att_conf_att_conf_id_seq', 1, false);


--
-- Name: att_conf_format_att_conf_format_id_seq; Type: SEQUENCE SET; Schema: public; Owner: {{ username }}
--

SELECT pg_catalog.setval('public.att_conf_format_att_conf_format_id_seq', 3, true);


--
-- Name: att_conf_type_att_conf_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: {{ username }}
--

SELECT pg_catalog.setval('public.att_conf_type_att_conf_type_id_seq', 14, true);


--
-- Name: att_conf_write_att_conf_write_id_seq; Type: SEQUENCE SET; Schema: public; Owner: {{ username }}
--

SELECT pg_catalog.setval('public.att_conf_write_att_conf_write_id_seq', 4, true);


--
-- Name: att_error_desc_att_error_desc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: {{ username }}
--

SELECT pg_catalog.setval('public.att_error_desc_att_error_desc_id_seq', 1, false);


--
-- Name: att_history_event_att_history_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: {{ username }}
--

SELECT pg_catalog.setval('public.att_history_event_att_history_event_id_seq', 1, false);


--
-- Name: att_array_devboolean att_array_devboolean_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devboolean
    ADD CONSTRAINT att_array_devboolean_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devdouble att_array_devdouble_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devdouble
    ADD CONSTRAINT att_array_devdouble_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devencoded att_array_devencoded_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devencoded
    ADD CONSTRAINT att_array_devencoded_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devenum att_array_devenum_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devenum
    ADD CONSTRAINT att_array_devenum_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devfloat att_array_devfloat_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devfloat
    ADD CONSTRAINT att_array_devfloat_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devlong64 att_array_devlong64_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devlong64
    ADD CONSTRAINT att_array_devlong64_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devlong att_array_devlong_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devlong
    ADD CONSTRAINT att_array_devlong_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devshort att_array_devshort_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devshort
    ADD CONSTRAINT att_array_devshort_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devstate att_array_devstate_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devstate
    ADD CONSTRAINT att_array_devstate_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devstring att_array_devstring_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devstring
    ADD CONSTRAINT att_array_devstring_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devuchar att_array_devuchar_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devuchar
    ADD CONSTRAINT att_array_devuchar_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devulong64 att_array_devulong64_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devulong64
    ADD CONSTRAINT att_array_devulong64_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devulong att_array_devulong_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devulong
    ADD CONSTRAINT att_array_devulong_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devushort att_array_devushort_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devushort
    ADD CONSTRAINT att_array_devushort_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_conf att_conf_att_name_key; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf
    ADD CONSTRAINT att_conf_att_name_key UNIQUE (att_name);


--
-- Name: att_conf_format att_conf_format_format_key; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_format
    ADD CONSTRAINT att_conf_format_format_key UNIQUE (format);


--
-- Name: att_conf_format att_conf_format_format_num_key; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_format
    ADD CONSTRAINT att_conf_format_format_num_key UNIQUE (format_num);


--
-- Name: att_conf_format att_conf_format_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_format
    ADD CONSTRAINT att_conf_format_pkey PRIMARY KEY (att_conf_format_id);


--
-- Name: att_conf att_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf
    ADD CONSTRAINT att_conf_pkey PRIMARY KEY (att_conf_id);


--
-- Name: att_conf_type att_conf_type_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_type
    ADD CONSTRAINT att_conf_type_pkey PRIMARY KEY (att_conf_type_id);


--
-- Name: att_conf_type att_conf_type_type_key; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_type
    ADD CONSTRAINT att_conf_type_type_key UNIQUE (type);


--
-- Name: att_conf_type att_conf_type_type_num_key; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_type
    ADD CONSTRAINT att_conf_type_type_num_key UNIQUE (type_num);


--
-- Name: att_conf_write att_conf_write_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_write
    ADD CONSTRAINT att_conf_write_pkey PRIMARY KEY (att_conf_write_id);


--
-- Name: att_conf_write att_conf_write_write_key; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_write
    ADD CONSTRAINT att_conf_write_write_key UNIQUE (write);


--
-- Name: att_conf_write att_conf_write_write_num_key; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf_write
    ADD CONSTRAINT att_conf_write_write_num_key UNIQUE (write_num);


--
-- Name: att_error_desc att_error_desc_error_desc_key; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_error_desc
    ADD CONSTRAINT att_error_desc_error_desc_key UNIQUE (error_desc);


--
-- Name: att_error_desc att_error_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_error_desc
    ADD CONSTRAINT att_error_desc_pkey PRIMARY KEY (att_error_desc_id);


--
-- Name: att_history_event att_history_event_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_history_event
    ADD CONSTRAINT att_history_event_pkey PRIMARY KEY (att_history_event_id);


--
-- Name: att_history att_history_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_history
    ADD CONSTRAINT att_history_pkey PRIMARY KEY (att_conf_id, event_time);


--
-- Name: att_scalar_devboolean att_scalar_devboolean_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devboolean
    ADD CONSTRAINT att_scalar_devboolean_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devdouble att_scalar_devdouble_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devdouble
    ADD CONSTRAINT att_scalar_devdouble_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devencoded att_scalar_devencoded_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devencoded
    ADD CONSTRAINT att_scalar_devencoded_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devenum att_scalar_devenum_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devenum
    ADD CONSTRAINT att_scalar_devenum_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devfloat att_scalar_devfloat_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devfloat
    ADD CONSTRAINT att_scalar_devfloat_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devlong64 att_scalar_devlong64_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devlong64
    ADD CONSTRAINT att_scalar_devlong64_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devlong att_scalar_devlong_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devlong
    ADD CONSTRAINT att_scalar_devlong_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devshort att_scalar_devshort_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devshort
    ADD CONSTRAINT att_scalar_devshort_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devstate att_scalar_devstate_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devstate
    ADD CONSTRAINT att_scalar_devstate_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devstring att_scalar_devstring_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devstring
    ADD CONSTRAINT att_scalar_devstring_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devuchar att_scalar_devuchar_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devuchar
    ADD CONSTRAINT att_scalar_devuchar_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devulong64 att_scalar_devulong64_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devulong64
    ADD CONSTRAINT att_scalar_devulong64_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devulong att_scalar_devulong_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devulong
    ADD CONSTRAINT att_scalar_devulong_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_scalar_devushort att_scalar_devushort_pkey; Type: CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devushort
    ADD CONSTRAINT att_scalar_devushort_pkey PRIMARY KEY (att_conf_id, data_time);


--
-- Name: att_array_devboolean_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devboolean_att_conf_id_data_time_idx ON public.att_array_devboolean USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devboolean_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devboolean_att_conf_id_idx ON public.att_array_devboolean USING btree (att_conf_id);


--
-- Name: att_array_devdouble_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devdouble_att_conf_id_data_time_idx ON public.att_array_devdouble USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devdouble_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devdouble_att_conf_id_idx ON public.att_array_devdouble USING btree (att_conf_id);


--
-- Name: att_array_devencoded_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devencoded_att_conf_id_data_time_idx ON public.att_array_devencoded USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devencoded_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devencoded_att_conf_id_idx ON public.att_array_devencoded USING btree (att_conf_id);


--
-- Name: att_array_devenum_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devenum_att_conf_id_data_time_idx ON public.att_array_devenum USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devenum_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devenum_att_conf_id_idx ON public.att_array_devenum USING btree (att_conf_id);


--
-- Name: att_array_devfloat_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devfloat_att_conf_id_data_time_idx ON public.att_array_devfloat USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devfloat_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devfloat_att_conf_id_idx ON public.att_array_devfloat USING btree (att_conf_id);


--
-- Name: att_array_devlong64_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devlong64_att_conf_id_data_time_idx ON public.att_array_devlong64 USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devlong64_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devlong64_att_conf_id_idx ON public.att_array_devlong64 USING btree (att_conf_id);


--
-- Name: att_array_devlong_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devlong_att_conf_id_data_time_idx ON public.att_array_devlong USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devlong_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devlong_att_conf_id_idx ON public.att_array_devlong USING btree (att_conf_id);


--
-- Name: att_array_devshort_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devshort_att_conf_id_data_time_idx ON public.att_array_devshort USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devshort_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devshort_att_conf_id_idx ON public.att_array_devshort USING btree (att_conf_id);


--
-- Name: att_array_devstate_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devstate_att_conf_id_data_time_idx ON public.att_array_devstate USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devstate_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devstate_att_conf_id_idx ON public.att_array_devstate USING btree (att_conf_id);


--
-- Name: att_array_devstring_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devstring_att_conf_id_data_time_idx ON public.att_array_devstring USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devstring_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devstring_att_conf_id_idx ON public.att_array_devstring USING btree (att_conf_id);


--
-- Name: att_array_devuchar_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devuchar_att_conf_id_data_time_idx ON public.att_array_devuchar USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devuchar_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devuchar_att_conf_id_idx ON public.att_array_devuchar USING btree (att_conf_id);


--
-- Name: att_array_devulong64_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devulong64_att_conf_id_data_time_idx ON public.att_array_devulong64 USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devulong64_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devulong64_att_conf_id_idx ON public.att_array_devulong64 USING btree (att_conf_id);


--
-- Name: att_array_devulong_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devulong_att_conf_id_data_time_idx ON public.att_array_devulong USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devulong_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devulong_att_conf_id_idx ON public.att_array_devulong USING btree (att_conf_id);


--
-- Name: att_array_devushort_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devushort_att_conf_id_data_time_idx ON public.att_array_devushort USING btree (att_conf_id, data_time DESC);


--
-- Name: att_array_devushort_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_array_devushort_att_conf_id_idx ON public.att_array_devushort USING btree (att_conf_id);


--
-- Name: att_history_att_history_event_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_history_att_history_event_id_idx ON public.att_history_event USING btree (att_history_event_id);


--
-- Name: att_parameter_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_parameter_att_conf_id_idx ON public.att_parameter USING btree (att_conf_id);


--
-- Name: att_parameter_recv_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_parameter_recv_time_idx ON public.att_parameter USING btree (recv_time);


--
-- Name: att_scalar_devboolean_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devboolean_att_conf_id_data_time_idx ON public.att_scalar_devboolean USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devboolean_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devboolean_att_conf_id_idx ON public.att_scalar_devboolean USING btree (att_conf_id);


--
-- Name: att_scalar_devdouble_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devdouble_att_conf_id_data_time_idx ON public.att_scalar_devdouble USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devdouble_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devdouble_att_conf_id_idx ON public.att_scalar_devdouble USING btree (att_conf_id);


--
-- Name: att_scalar_devencoded_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devencoded_att_conf_id_data_time_idx ON public.att_scalar_devencoded USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devencoded_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devencoded_att_conf_id_idx ON public.att_scalar_devencoded USING btree (att_conf_id);


--
-- Name: att_scalar_devenum_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devenum_att_conf_id_data_time_idx ON public.att_scalar_devenum USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devenum_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devenum_att_conf_id_idx ON public.att_scalar_devenum USING btree (att_conf_id);


--
-- Name: att_scalar_devfloat_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devfloat_att_conf_id_data_time_idx ON public.att_scalar_devfloat USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devfloat_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devfloat_att_conf_id_idx ON public.att_scalar_devfloat USING btree (att_conf_id);


--
-- Name: att_scalar_devlong64_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devlong64_att_conf_id_data_time_idx ON public.att_scalar_devlong64 USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devlong64_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devlong64_att_conf_id_idx ON public.att_scalar_devlong64 USING btree (att_conf_id);


--
-- Name: att_scalar_devlong_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devlong_att_conf_id_data_time_idx ON public.att_scalar_devlong USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devlong_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devlong_att_conf_id_idx ON public.att_scalar_devlong USING btree (att_conf_id);


--
-- Name: att_scalar_devshort_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devshort_att_conf_id_data_time_idx ON public.att_scalar_devshort USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devshort_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devshort_att_conf_id_idx ON public.att_scalar_devshort USING btree (att_conf_id);


--
-- Name: att_scalar_devstate_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devstate_att_conf_id_data_time_idx ON public.att_scalar_devstate USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devstate_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devstate_att_conf_id_idx ON public.att_scalar_devstate USING btree (att_conf_id);


--
-- Name: att_scalar_devstring_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devstring_att_conf_id_data_time_idx ON public.att_scalar_devstring USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devstring_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devstring_att_conf_id_idx ON public.att_scalar_devstring USING btree (att_conf_id);


--
-- Name: att_scalar_devuchar_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devuchar_att_conf_id_data_time_idx ON public.att_scalar_devuchar USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devuchar_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devuchar_att_conf_id_idx ON public.att_scalar_devuchar USING btree (att_conf_id);


--
-- Name: att_scalar_devulong64_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devulong64_att_conf_id_data_time_idx ON public.att_scalar_devulong64 USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devulong64_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devulong64_att_conf_id_idx ON public.att_scalar_devulong64 USING btree (att_conf_id);


--
-- Name: att_scalar_devulong_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devulong_att_conf_id_data_time_idx ON public.att_scalar_devulong USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devulong_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devulong_att_conf_id_idx ON public.att_scalar_devulong USING btree (att_conf_id);


--
-- Name: att_scalar_devushort_att_conf_id_data_time_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devushort_att_conf_id_data_time_idx ON public.att_scalar_devushort USING btree (att_conf_id, data_time DESC);


--
-- Name: att_scalar_devushort_att_conf_id_idx; Type: INDEX; Schema: public; Owner: {{ username }}
--

CREATE INDEX att_scalar_devushort_att_conf_id_idx ON public.att_scalar_devushort USING btree (att_conf_id);


--
-- Name: att_array_devenum enum_label_trigger; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

CREATE TRIGGER enum_label_trigger BEFORE INSERT ON public.att_array_devenum FOR EACH ROW EXECUTE FUNCTION public.set_enum_label_array();


--
-- Name: att_scalar_devenum enum_label_trigger; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

CREATE TRIGGER enum_label_trigger BEFORE INSERT ON public.att_scalar_devenum FOR EACH ROW EXECUTE FUNCTION public.set_enum_label();


--
-- Name: att_array_devboolean ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devboolean FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devdouble ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devdouble FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devencoded ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devencoded FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devenum ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devenum FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devfloat ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devfloat FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devlong ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devlong FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devlong64 ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devlong64 FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devshort ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devshort FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devstate ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devstate FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devstring ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devstring FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devuchar ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devuchar FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devulong ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devulong FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devulong64 ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devulong64 FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devushort ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_array_devushort FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_parameter ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_parameter FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devboolean ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devboolean FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devdouble ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devdouble FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devencoded ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devencoded FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devenum ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devenum FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devfloat ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devfloat FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devlong ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devlong FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devlong64 ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devlong64 FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devshort ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devshort FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devstate ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devstate FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devstring ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devstring FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devuchar ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devuchar FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devulong ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devulong FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devulong64 ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devulong64 FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_scalar_devushort ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: {{ username }}
--

--CREATE TRIGGER public.ts_insert_blocker BEFORE INSERT ON att_scalar_devushort FOR EACH ROW EXECUTE FUNCTION public._timescaledb_internal.insert_blocker();


--
-- Name: att_array_devboolean att_array_devboolean_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devboolean
    ADD CONSTRAINT att_array_devboolean_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devboolean att_array_devboolean_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devboolean
    ADD CONSTRAINT att_array_devboolean_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devdouble att_array_devdouble_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devdouble
    ADD CONSTRAINT att_array_devdouble_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devdouble att_array_devdouble_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devdouble
    ADD CONSTRAINT att_array_devdouble_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devencoded att_array_devencoded_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devencoded
    ADD CONSTRAINT att_array_devencoded_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devencoded att_array_devencoded_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devencoded
    ADD CONSTRAINT att_array_devencoded_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devenum att_array_devenum_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devenum
    ADD CONSTRAINT att_array_devenum_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devenum att_array_devenum_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devenum
    ADD CONSTRAINT att_array_devenum_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devfloat att_array_devfloat_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devfloat
    ADD CONSTRAINT att_array_devfloat_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devfloat att_array_devfloat_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devfloat
    ADD CONSTRAINT att_array_devfloat_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devlong64 att_array_devlong64_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devlong64
    ADD CONSTRAINT att_array_devlong64_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devlong64 att_array_devlong64_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devlong64
    ADD CONSTRAINT att_array_devlong64_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devlong att_array_devlong_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devlong
    ADD CONSTRAINT att_array_devlong_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devlong att_array_devlong_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devlong
    ADD CONSTRAINT att_array_devlong_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devshort att_array_devshort_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devshort
    ADD CONSTRAINT att_array_devshort_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devshort att_array_devshort_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devshort
    ADD CONSTRAINT att_array_devshort_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devstate att_array_devstate_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devstate
    ADD CONSTRAINT att_array_devstate_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devstate att_array_devstate_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devstate
    ADD CONSTRAINT att_array_devstate_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devstring att_array_devstring_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devstring
    ADD CONSTRAINT att_array_devstring_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devstring att_array_devstring_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devstring
    ADD CONSTRAINT att_array_devstring_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devuchar att_array_devuchar_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devuchar
    ADD CONSTRAINT att_array_devuchar_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devuchar att_array_devuchar_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devuchar
    ADD CONSTRAINT att_array_devuchar_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devulong64 att_array_devulong64_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devulong64
    ADD CONSTRAINT att_array_devulong64_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devulong64 att_array_devulong64_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devulong64
    ADD CONSTRAINT att_array_devulong64_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devulong att_array_devulong_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devulong
    ADD CONSTRAINT att_array_devulong_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devulong att_array_devulong_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devulong
    ADD CONSTRAINT att_array_devulong_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_array_devushort att_array_devushort_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devushort
    ADD CONSTRAINT att_array_devushort_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_array_devushort att_array_devushort_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_array_devushort
    ADD CONSTRAINT att_array_devushort_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_conf att_conf_att_conf_format_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf
    ADD CONSTRAINT att_conf_att_conf_format_id_fkey FOREIGN KEY (att_conf_format_id) REFERENCES public.att_conf_format(att_conf_format_id);


--
-- Name: att_conf att_conf_att_conf_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf
    ADD CONSTRAINT att_conf_att_conf_type_id_fkey FOREIGN KEY (att_conf_type_id) REFERENCES public.att_conf_type(att_conf_type_id);


--
-- Name: att_conf att_conf_att_conf_write_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_conf
    ADD CONSTRAINT att_conf_att_conf_write_id_fkey FOREIGN KEY (att_conf_write_id) REFERENCES public.att_conf_write(att_conf_write_id);


--
-- Name: att_history att_history_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_history
    ADD CONSTRAINT att_history_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_history att_history_att_history_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_history
    ADD CONSTRAINT att_history_att_history_event_id_fkey FOREIGN KEY (att_history_event_id) REFERENCES public.att_history_event(att_history_event_id);


--
-- Name: att_parameter att_parameter_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_parameter
    ADD CONSTRAINT att_parameter_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devboolean att_scalar_devboolean_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devboolean
    ADD CONSTRAINT att_scalar_devboolean_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devboolean att_scalar_devboolean_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devboolean
    ADD CONSTRAINT att_scalar_devboolean_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devdouble att_scalar_devdouble_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devdouble
    ADD CONSTRAINT att_scalar_devdouble_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devdouble att_scalar_devdouble_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devdouble
    ADD CONSTRAINT att_scalar_devdouble_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devencoded att_scalar_devencoded_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devencoded
    ADD CONSTRAINT att_scalar_devencoded_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devencoded att_scalar_devencoded_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devencoded
    ADD CONSTRAINT att_scalar_devencoded_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devenum att_scalar_devenum_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devenum
    ADD CONSTRAINT att_scalar_devenum_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devenum att_scalar_devenum_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devenum
    ADD CONSTRAINT att_scalar_devenum_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devfloat att_scalar_devfloat_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devfloat
    ADD CONSTRAINT att_scalar_devfloat_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devfloat att_scalar_devfloat_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devfloat
    ADD CONSTRAINT att_scalar_devfloat_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devlong64 att_scalar_devlong64_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devlong64
    ADD CONSTRAINT att_scalar_devlong64_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devlong64 att_scalar_devlong64_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devlong64
    ADD CONSTRAINT att_scalar_devlong64_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devlong att_scalar_devlong_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devlong
    ADD CONSTRAINT att_scalar_devlong_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devlong att_scalar_devlong_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devlong
    ADD CONSTRAINT att_scalar_devlong_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devshort att_scalar_devshort_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devshort
    ADD CONSTRAINT att_scalar_devshort_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devshort att_scalar_devshort_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devshort
    ADD CONSTRAINT att_scalar_devshort_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devstate att_scalar_devstate_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devstate
    ADD CONSTRAINT att_scalar_devstate_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devstate att_scalar_devstate_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devstate
    ADD CONSTRAINT att_scalar_devstate_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devstring att_scalar_devstring_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devstring
    ADD CONSTRAINT att_scalar_devstring_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devstring att_scalar_devstring_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devstring
    ADD CONSTRAINT att_scalar_devstring_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devuchar att_scalar_devuchar_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devuchar
    ADD CONSTRAINT att_scalar_devuchar_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devuchar att_scalar_devuchar_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devuchar
    ADD CONSTRAINT att_scalar_devuchar_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devulong64 att_scalar_devulong64_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devulong64
    ADD CONSTRAINT att_scalar_devulong64_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devulong64 att_scalar_devulong64_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devulong64
    ADD CONSTRAINT att_scalar_devulong64_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devulong att_scalar_devulong_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devulong
    ADD CONSTRAINT att_scalar_devulong_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devulong att_scalar_devulong_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devulong
    ADD CONSTRAINT att_scalar_devulong_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: att_scalar_devushort att_scalar_devushort_att_conf_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devushort
    ADD CONSTRAINT att_scalar_devushort_att_conf_id_fkey FOREIGN KEY (att_conf_id) REFERENCES public.att_conf(att_conf_id);


--
-- Name: att_scalar_devushort att_scalar_devushort_att_error_desc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: {{ username }}
--

ALTER TABLE ONLY public.att_scalar_devushort
    ADD CONSTRAINT att_scalar_devushort_att_error_desc_id_fkey FOREIGN KEY (att_error_desc_id) REFERENCES public.att_error_desc(att_error_desc_id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: {{ username }}
--

GRANT USAGE ON SCHEMA public TO readonly;
GRANT ALL ON SCHEMA public TO readwrite;


--
-- Name: TABLE public.att_array_devboolean; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devboolean TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devboolean TO readwrite;


--
-- Name: TABLE public.att_array_devdouble; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devdouble TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devdouble TO readwrite;


--
-- Name: TABLE public.att_array_devencoded; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devencoded TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devencoded TO readwrite;


--
-- Name: TABLE public.att_array_devenum; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devenum TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devenum TO readwrite;


--
-- Name: TABLE public.att_array_devfloat; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devfloat TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devfloat TO readwrite;


--
-- Name: TABLE public.att_array_devlong; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devlong TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devlong TO readwrite;


--
-- Name: TABLE public.att_array_devlong64; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devlong64 TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devlong64 TO readwrite;


--
-- Name: TABLE public.att_array_devshort; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devshort TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devshort TO readwrite;


--
-- Name: TABLE public.att_array_devstate; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devstate TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devstate TO readwrite;


--
-- Name: TABLE public.att_array_devstring; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devstring TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devstring TO readwrite;


--
-- Name: TABLE public.att_array_devuchar; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devuchar TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devuchar TO readwrite;


--
-- Name: TABLE public.att_array_devulong; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devulong TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devulong TO readwrite;


--
-- Name: TABLE public.att_array_devulong64; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devulong64 TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devulong64 TO readwrite;


--
-- Name: TABLE public.att_array_devushort; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_array_devushort TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_array_devushort TO readwrite;


--
-- Name: TABLE public.att_conf; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_conf TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_conf TO readwrite;


--
-- Name: SEQUENCE att_conf_att_conf_id_seq; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT USAGE ON SEQUENCE public.att_conf_att_conf_id_seq TO readwrite;


--
-- Name: TABLE public.att_conf_format; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_conf_format TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_conf_format TO readwrite;


--
-- Name: SEQUENCE att_conf_format_att_conf_format_id_seq; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT USAGE ON SEQUENCE public.att_conf_format_att_conf_format_id_seq TO readwrite;


--
-- Name: TABLE public.att_conf_type; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_conf_type TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_conf_type TO readwrite;


--
-- Name: SEQUENCE att_conf_type_att_conf_type_id_seq; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT USAGE ON SEQUENCE public.att_conf_type_att_conf_type_id_seq TO readwrite;


--
-- Name: TABLE public.att_conf_write; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_conf_write TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_conf_write TO readwrite;


--
-- Name: SEQUENCE att_conf_write_att_conf_write_id_seq; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT USAGE ON SEQUENCE public.att_conf_write_att_conf_write_id_seq TO readwrite;


--
-- Name: TABLE public.att_error_desc; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_error_desc TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_error_desc TO readwrite;


--
-- Name: SEQUENCE att_error_desc_att_error_desc_id_seq; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT USAGE ON SEQUENCE public.att_error_desc_att_error_desc_id_seq TO readwrite;


--
-- Name: TABLE public.att_history; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_history TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_history TO readwrite;


--
-- Name: TABLE public.att_history_event; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_history_event TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_history_event TO readwrite;


--
-- Name: SEQUENCE att_history_event_att_history_event_id_seq; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT USAGE ON SEQUENCE public.att_history_event_att_history_event_id_seq TO readwrite;


--
-- Name: TABLE public.att_parameter; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_parameter TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_parameter TO readwrite;


--
-- Name: TABLE public.att_scalar_devboolean; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devboolean TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devboolean TO readwrite;


--
-- Name: TABLE public.att_scalar_devdouble; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devdouble TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devdouble TO readwrite;


--
-- Name: TABLE public.att_scalar_devencoded; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devencoded TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devencoded TO readwrite;


--
-- Name: TABLE public.att_scalar_devenum; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devenum TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devenum TO readwrite;


--
-- Name: TABLE public.att_scalar_devfloat; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devfloat TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devfloat TO readwrite;


--
-- Name: TABLE public.att_scalar_devlong; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devlong TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devlong TO readwrite;


--
-- Name: TABLE public.att_scalar_devlong64; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devlong64 TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devlong64 TO readwrite;


--
-- Name: TABLE public.att_scalar_devshort; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devshort TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devshort TO readwrite;


--
-- Name: TABLE public.att_scalar_devstate; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devstate TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devstate TO readwrite;


--
-- Name: TABLE public.att_scalar_devstring; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devstring TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devstring TO readwrite;


--
-- Name: TABLE public.att_scalar_devuchar; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devuchar TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devuchar TO readwrite;


--
-- Name: TABLE public.att_scalar_devulong; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devulong TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devulong TO readwrite;


--
-- Name: TABLE public.att_scalar_devulong64; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devulong64 TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devulong64 TO readwrite;


--
-- Name: TABLE public.att_scalar_devushort; Type: ACL; Schema: public; Owner: {{ username }}
--

GRANT SELECT ON TABLE public.att_scalar_devushort TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.att_scalar_devushort TO readwrite;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: public; Owner: {{ username }}
--

ALTER DEFAULT PRIVILEGES FOR ROLE {{ username }} IN SCHEMA public GRANT USAGE ON SEQUENCES  TO readwrite;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: public; Owner: {{ username }}
--

ALTER DEFAULT PRIVILEGES FOR ROLE {{ username }} IN SCHEMA public GRANT SELECT ON TABLES  TO readonly;
ALTER DEFAULT PRIVILEGES FOR ROLE {{ username }} IN SCHEMA public GRANT SELECT,INSERT,DELETE,UPDATE ON TABLES  TO readwrite;


--
-- PostgreSQL database dump complete
--

