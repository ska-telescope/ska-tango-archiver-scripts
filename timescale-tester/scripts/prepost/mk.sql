CREATE DATABASE {{ database }};
GRANT CONNECT ON DATABASE {{ database }} TO {{ username }};
\c {{ database }}
GRANT CREATE ON SCHEMA public TO {{ username }};
GRANT CREATE ON SCHEMA public TO admin;


CREATE ROLE readonly;
CREATE ROLE readwrite;
GRANT CREATE ON SCHEMA public to readonly;
GRANT CREATE ON SCHEMA public to readonly;

-- Permissions - readonly
GRANT CONNECT ON DATABASE {{ database }} TO readonly;
GRANT USAGE ON SCHEMA public TO readonly;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO readonly;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO readonly;

-- Permissions - readwrite
GRANT CONNECT ON DATABASE {{ database }} TO readwrite;
GRANT USAGE ON SCHEMA public TO readwrite;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO readwrite;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO readwrite;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO readwrite;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE ON SEQUENCES TO readwrite;
GRANT ALL ON SCHEMA public TO readwrite;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO readwrite;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO readwrite;

CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;

--set search_path to {{database}}, public;



