#!/usr/bin/env python3

import argparse
import psycopg2
from jinja2 import Environment, FileSystemLoader


sqlScripts = ["01-hdb.sql", "mkhyper.sql"]#,"02-ts.sql"]
mkScript = ["prepost/mk.sql"]
delScript= ["prepost/del.sql"]
compScript = ["03-compress.sql"]
mkUser = ["prepost/mkuser.sql"]
delUser = ["prepost/deluser.sql"]

environment = Environment(loader=FileSystemLoader("./"))

def runDel(args):
    content = ""
    for t in delScript:
        template = environment.get_template(t)
        contentTmp = template.render(database = args.database)
        content = "%s%s"%(content, contentTmp)
    for t in delUser:
        template = environment.get_template(t)
        contentTmp = template.render(username = args.user)
        content = "%s%s"%(content, contentTmp)
    print(content)
    return content

def makeSQL(args):
    content = ""
    if args.create:
        #Create user
        for t in mkUser:
            template = environment.get_template(t)
            contentTmp = template.render(database = args.database, username = args.user, userpass = args.userpass)
            content = "%s%s"%(content, contentTmp)
        #Creation of tables
        for t in mkScript:
            template = environment.get_template(t)
            contentTmp = template.render(database = args.database, username = args.user)
            content = "%s%s"%(content, contentTmp)
    for t in sqlScripts:
        #SQL
        template = environment.get_template(t)
        contentTmp = template.render(database = args.database, username = args.user, interval = args.interval)
        content = "%s%s"%(content, contentTmp)
    if args.compression:
        #Compression
        for t in compScript:
            template = environment.get_template(t)
            contentTmp = template.render(database = args.database, interval = args.interval, username = args.user)
            content = "%s%s"%(content, contentTmp)
    if not args.doIt:
        print(content)
    return content     

def run_new_db_command(args):
    """
    Handler for argparse
    """

    # first attempt to open a connection to the database
    try:
        SQL = makeSQL(args)
        if verbose and args.doIt:
            print(("Attempting to connect to server. Connect: {}".format(args.connect)))

        # attempt to connect to the server
        if args.doIt:
            connection = psycopg2.connect(args.connect)
            connection.autocommit = False

            if verbose:
                print("Connected to database at server")

    except (Exception, psycopg2.Error) as error:
        print(("Error: {}".format(error)))
        return False



    return True

def main():
    parser = argparse.ArgumentParser(description="Create a database to test for a specific user")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose execution")

    parser.add_argument(
        "-c",
        "--connect",
        metavar="STR",
        default="user=postgres dbname=zero1",
        help="connect string (default user=postgres dbname=zero1)"
    )
    parser.add_argument("--database", default="zero1", help="Database name, default is zero1")
    parser.add_argument("--doIt", action="store_true", help="Run the command, otherwise output the SQL")
    parser.add_argument("--user", default="test1", help="User role, default is test1")

    subparsers = parser.add_subparsers(title="available commands", metavar="mode")

    parser_new_db = subparsers.add_parser("genSQL", help="create the script")
    parser_new_db.add_argument("--userpass", default="user4test", help="User role, default is user4test")
    parser_new_db.add_argument("--create", action="store_true", help="Creates the datbase too.")
    parser_new_db.add_argument("--compression", action="store_true", help="Enables compression on data")
    parser_new_db.add_argument("--interval", default="1h", help="Compression chunks, default is 1h")
    
    parser_new_db.set_defaults(func=run_new_db_command)

    parser_data = subparsers.add_parser("remove", help="Remove the database")
    parser_data.set_defaults(func=runDel)

    args = parser.parse_args()

    global verbose
    verbose = args.verbose

    return args.func(args)

if __name__ == "__main__":
    main()
