\c {{ database }}
SELECT create_hypertable('att_parameter', 'recv_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devboolean', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devboolean', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devuchar', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devuchar', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devshort', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devshort', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devushort', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devushort', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devlong', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devlong', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devulong', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devulong', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devlong64', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devlong64', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devulong64', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devulong64', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devfloat', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devfloat', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devdouble', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devdouble', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devstring', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devstring', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devstate', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devstate', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devencoded', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devencoded', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_scalar_devenum', 'data_time', chunk_time_interval => interval '{{ interval }}');
SELECT create_hypertable('att_array_devenum', 'data_time', chunk_time_interval => interval '{{ interval }}');

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO {{ username }};
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO {{ username }};

