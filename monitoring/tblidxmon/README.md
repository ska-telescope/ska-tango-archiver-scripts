# Table and index monitoring

This is a prometheus exporter based on sql_exporter to monitor tables and indexes growth. Queries are defined on the pg.collector.yml. This uses burningalchemist/sql_exporter and it is for PostgreSQL. 

## Setup 

### Config file

Change the data_source_name on sql_exporter.yml to use it.

### Prometheus setup

Prometheus scrapper config, use the container on port 9399:

  - job_name: sql_exporter
    # SQL exporter
    static_configs:
      - targets: ['172.17.0.2:9399']

### Grafana Dashboard

Import gf.json
