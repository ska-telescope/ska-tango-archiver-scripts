#!/bin/sh
docker build -t myexporter .

docker run --name myexprun -v $PWD/sql_exporter.yml:/etc/sql_exporter/sql_exporter.yml -v $PWD/pg.collector.yml:/etc/sql_exporter/pg.collector.yml -d -t myexporter
